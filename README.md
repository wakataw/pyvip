# PyVIP
[Bitcoin Indonesia API](https://vip.bitcoin.co.id/downloads/BITCOINCOID-API-DOCUMENTATION.pdf) Python API Wrapper

## Instalasi

Git:

```bash
$ pip install -e git+https://gitlab.com/wakataw/pyvip#egg=pyvip
```

Paket `pyvip` hanya tersedia untuk Python 3

## Bitcoin.co.id API

1. [Public API](#public-api)

    Public API dapat diakses oleh umum tanpa harus memiliki API Key.
2. [Private API](#private-api)

    Untuk dapat menggunakan Private API, anda harus mengaktifkan Trade API terlebih dahulu pada halaman [Bitcon.co.id](https://vip.bitcoin.co.id/trade_api) untuk mendapatkan API Key dan API Secret

## Public API

### Usage

```python
from pyvip import public
```

### Method
* [public.**ticker**](#publicticker)
* [public.**depth**](#publicdepth)
* [public.**trades**](#publictrades)
* [public.**history**](#publichistory)

### public.ticker

> Menampilkan nilai terkini dari salah satu mata uang.

Parameter

Parameter|Diperlukan|Keterangan|Nilai|Default
-|-|-|-|-
`pair`|Ya|Pasangan simbol mata uang|`btc_idr`, `ltc_btc`, `xlm_idr`, etc|`btc_idr`


contoh:

```python
>>> public.ticker(pair='btc_idr')
```

return:

```json
{
  "ticker": {
    "high": "228339000",
    "low": "212500000",
    "vol_btc": "754.44424019",
    "vol_idr": "165692182771",
    "last": "215489000",
    "buy": "215484000",
    "sell": "215489000",
    "server_time": 1515692665
  }
}
```

### public.depth

> Menampilkan daftar order jual dan beli suatu mata uang

Parameter:

Parameter|Diperlukan|Keterangan|Nilai|Default
-|-|-|-|-
`pair`|Ya|Pasangan simbol mata uang|`btc_idr`, `ltc_btc`, `xlm_idr`, etc|`btc_idr`

contoh:
```python
>>> public.depth(pair='btc_idr')
```

return:

```json
{
  "buy": [
    [215501000, "0.09470211"], 
    [215500000, "0.26273596"], 
    [215451000, "0.06497997"], 
    [215450000, "0.08486187"]
  ], 
  "sell": [
    [215502000, "0.01076496"], 
    [215653000, "0.01500705"], 
    [215654000, "0.00046509"], 
    [215663000, "0.00037657"]
  ]
}
```

### public.trades

> Menampilkan 150 transaksi jual beli terakhir suatu mata uang

Parameter:

Parameter|Diperlukan|Keterangan|Nilai|Default
-|-|-|-|-
`pair`|Ya|Pasangan simbol mata uang|`btc_idr`, `ltc_btc`, `xlm_idr`, etc|`btc_idr`

contoh:
```python
>>> public.trades(pair='btc_idr')
```

return:

```json
[
  {
    "date": "1515693877",
    "price": "215881000",
    "amount": "0.01897524",
    "tid": "5492964",
    "type": "buy"
  },
  {
    "date": "1515693877",
    "price": "215880000",
    "amount": "0.05364716",
    "tid": "5492963",
    "type": "buy"
  },
  {
    "date": "1515693862",
    "price": "215880000",
    "amount": "0.01396651",
    "tid": "5492962",
    "type": "buy"
  },
  {
    "date": "1515693849",
    "price": "215879000",
    "amount": "0.00126231",
    "tid": "5492961",
    "type": "sell"
  },
  ...
]  
```

### public.history

> Menampilkan riwayat ticker suatu mata uang dalam rentang waktu tertentu

parameter

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`start_time`|Ya|Waktu awal data|`(int) UNIX Time`|-
`end_time`|Ya|Waktu akhir riwayat| `(int) UNIX Time` | -
`resolution`|Ya|Rentang waktu antar data | `'1'` : 1 menit<br>`'5'` : 5 menit<br>`'15'` : 15 menit<br>`'60'` : 1 jam <br>`'D` : 1 hari | `'D'` (1 hari)
`pair`|tidak|waktu awal, range filter berdasarkan waktu|`UNIX time`|-

contoh:
```python
>>> # menampilkan data riwayat harga BTC/IDR per hari mulai dari tanggal 7 Januari 2018 sampai dengan sekarang
>>> 
>>> start_time = int(time.mktime(datetime(2018, 1, 7).timetuple()))
>>> end_time = int(time.mktime(datetime.now().timetuple()))
>>> 
>>> public.history(start_time=start_time, end_time=end_time, resolution='D', pair='btc_idr')
```

return:
```json
{
  "s": "ok",
  "t": [
    1515283200,
    1515369600,
    1515456000,
    1515542400,
    1515628800
  ],
  "c": [
    245980000,
    237575000,
    236489000,
    226740000,
    215501000
  ],
  "o": [
    249399000,
    245980000,
    237575000,
    236489000,
    226740000
  ],
  "h": [
    250199000,
    249000000,
    243950000,
    236492000,
    228339000
  ],
  "l": [
    242755000,
    230000000,
    234518000,
    211366000,
    212500000
  ],
  "v": [
    510.3434618,
    789.28274644,
    481.7751198,
    1036.51561343,
    691.97017128
  ]
}
```



## Private API


### Usage

```python
import pyvip

client = pyvip.Client('YOUR_API_KEY', 'YOUR_API_SECRET')
```

### Method

Permission|Method
---|---
`VIEW`|[get_info](#get-info), [trans_history](#transaction-history), [trade_history](#trade-history), [order_history](#order-history), [get_order](#get-order)
`TRADE`| [trade](#trade), [cancel_order](#cancel-order)

### Get Info

> Menampilkan saldo pada akun user

contoh:

```bash
>>> client.get_info()
```

return 

```json
{
  "success": 1,
  "return": {
    "address": {
      "bch": "",
      "btc": "1GVzKLwVufex1p1zRPUDwjWkDj2S1RAjfm",
      "btg": "",
      "bts": "bitcoinindonesia",
      "doge": "",
      "drk": "",
      "etc": "",
      "eth": "0xda6g2aa36bc425db51dc47dfce477e16286e1671",
      "ignis": "ARDOR-9NCC-FSSB-44UM-C4XPN",
      "ltc": "",
      "nem": "NCCFO5QDFV5FS3BTBPEU2QO6UHZD7PCGFNCPISDL",
      "nxt": "NXT-8PMW-RW5T-NCRX-FBX4E",
      "str": "GC4KAS6W2YCGDGLP633A6F6AKTCV4WSLMTMIQRSEQE5QRRVKSX7THV6S",
      "waves": "3P2DooYJpPiQYZrok3GG69Yy39xcU3ZSEhG",
      "xrp": "rwWr7KUZ3ZFwzgaFGjKBysADByzxvohQ3C",
      "xzc": ""
    },
    "balance": {
      "bch": "0.00000000",
      "btc": "1.00000000",
      "btg": "0.25502820",
      "bts": "150.00000000",
      "doge": "0.00000000",
      "drk": "0.00000000",
      "etc": "0.00000000",
      "eth": "0.00000000",
      "idr": 0,
      "ignis": "0.00000000",
      "ltc": "2.00000033",
      "nem": "0.00000000",
      "nxt": "738.84331735",
      "str": "0.00000000",
      "waves": "0.00000000",
      "xrp": "0.00000000",
      "xzc": "0.00000000"
    },
    "balance_hold": {
      "bch": "0.00000000",
      "btc": "0.00000000",
      "btg": "0.00000000",
      "bts": "0.00000000",
      "doge": "0.00000000",
      "drk": "0.00000000",
      "etc": "0.00000000",
      "eth": "0.00000000",
      "idr": 0,
      "ignis": "0.00000000",
      "ltc": "0.00000000",
      "nem": "0.00000000",
      "nxt": "282.28747903",
      "str": "563.05377551",
      "waves": "0.00000000",
      "xrp": "0.00000000",
      "xzc": "0.69803478"
    },
    "email": "foo@bar.com",
    "name": "FOO BAR",
    "profile_picture": "https://s3.amazonaws.com/bitcoin.co.id/foobar.jpg",
    "server_time": 1515725471,
    "user_id": "666666"
  }
}

```
&nbsp;

#### Transaction History

> Menampilkan riwayat deposit dan penarikan (Rupiah dan _Cryptocurrency_)

contoh:

```python
>>> client.trans_history()
```

return:

```json
{
  "success": 1,
  "return": {
    "withdraw": {
      "idr": [
        {
          "status": "wait",
          "type": "coupon",
          "rp": "100000",
          "fee": "0",
          "amount": "100000",
          "submit_time": "1392135074",
          "success_time": "0"
        }
      ],
      "btc": [
        {
          "status": "success",
          "btc": "150000000",
          "fee": "20000",
          "amount": "149980000",
          "submit_time": "1392135074",
          "success_time": "0"
        }
      ],
      "ltc": [
      ],
      "other_coin": [
      ],
    },
    "deposit": {
      "idr": [
        {
          "status": "success",
          "type": "bank",
          "rp": "10000000",
          "fee": "0",
          "amount": "10000000",
          "submit_time": "1392193569",
          "success_time": "1392193569"
        }
      ],
      "btc": [
        {
          "status": "success",
          "btc": "200000000",
          "amount": "200000000",
          "success_time": "1391979201"
        }
      ],
      "ltc": [
      ],
      "other_coin": [
      ]
    }
  }
}
```
&nbsp;

#### Trade

> Memasang order jual atau beli

**Parameter**

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`pair`|Ya|Pasangan simbol mata uang|`btc_idr`, `ltc_str`, `str_btc`, `doge_btc`, dll|-
`trx_type`|Ya|Tipe transaksi|`buy` or `sell`| -
`price`|Ya|Harga |`float`|-
`amount`|Ya |**Altcoin**<br>Untuk transaksi altcoin, _amount_ adalah jumlah altcoin yang ingin dijual/dibeli<br><br>**Bitcoin**<br>**buy**: untuk transaksi _buy_, _amount_ adalah jumlah Rupiah untuk membeli Bitcoin<br><br>**sell**: Untuk transaksi _sell_, _amount_ adalah jumlah bitcoin yang ingin dijual|`float`|-

contoh altcoin:

```python
>>> # menjual 1000 DOGE dengan harga 0.00000100 BTC/DOGE
>>> client.trade(pair='nxt_idr', trx_type='sell', price=10000, amount=100)
```

```python
>>> # membeli 0.5 ETHEREUM dengan harga 19.000.000 IDR/ETH
>>> client.trade(pair='eth_idr', trx_type='buy', price=19000000.0, amount=0.5)
```

contoh bitcoin:

```python
>>> # membeli Bitcoin sejumlah IDR 10.000.000 pada harga 230.000.000 IDR/BTC
>>> client.trade(pair='btc_idr', trx_type='buy', price=230000000.0, amount=10000000.0)
```

```python
>>> # menjual Bitcoin sejumlah 0.5 BTC pada harga 250.000.000 IDR/BTC
>>> client.trade(pair='btc_idr', trx_type='sell', price=250000000.0, amount=0.5)
```

return `sell`:

catatan:
* `receive_` diikuti oleh jenis pasar. `recieve_rp` untuk pasar IDR dan `receive_btc` untuk pasar BTC
* `remain_` dan `sold_` diikut oleh simbol koin yang sedang dijual. Contoh: `remain_nxt`, `remain_xrp`, `sold_xlm`, dll

```json
{
  "success": 1,

  "return": {
    "fee": 0,
    "order_id": 1234567,
    "receive_rp": 0,
    "remain_nxt": "100.00000000",
    "sold_nxt": "0.00000000",
    "balance": {
      "bch": "0.00000000",
      "btc": "0.00000000",
      "btg": "0.00000000",
      "bts": "0.00000000",
      "doge": "0.00000000",
      "drk": "0.00000000",
      "etc": "0.00000000",
      "eth": "0.00000000",
      "frozen_bch": "0.00000000",
      "frozen_btc": "0.00000000",
      "frozen_btg": "0.00000000",
      "frozen_bts": "0.00000000",
      "frozen_doge": "0.00000000",
      "frozen_drk": "0.00000000",
      "frozen_etc": "0.00000000",
      "frozen_eth": "0.00000000",
      "frozen_idr": "0",
      "frozen_ignis": "0.00000000",
      "frozen_ltc": "0.00000000",
      "frozen_nem": "0.00000000",
      "frozen_nxt": "0.00000000",
      "frozen_str": "0.00000000",
      "frozen_waves": "0.00000000",
      "frozen_xrp": "0.00000000",
      "frozen_xzc": "0.00000000",
      "idr": "0",
      "ignis": "0.00000000",
      "ltc": "0.00000000",
      "nem": "0.00000000",
      "nxt": "0.00000000",
      "str": "0.00000000",
      "waves": "0.00000000",
      "xrp": "0.00000000",
      "xzc": "0.00000000"
    }
  }
}
```

return `buy`:

catatan:
* `receive_` diikuti oleh simbol koin yang dibeli. Contoh: `receive_eth`, `receive_xlm`, dll.
* `remain_` diikuti oleh jenis pasar. `remain_btc` jika membeli koin di pasar BTC dan `remain_rp` jika di pasar IDR.

```json
{
  "success": 1,
  "return": { 
    "fee": 0,
    "order_id": 1234567,
    "receive_eth": 0,
    "remain_rp": "19000000",
    "balance": {
      "bch": "0.00000000",
      "btc": "0.00000000",
      "btg": "0.00000000",
      "bts": "0.00000000",
      "doge": "0.00000000",
      "drk": "0.00000000",
      "etc": "0.00000000",
      "eth": "0.00000000",
      "frozen_bch": "0.00000000",
      "frozen_btc": "0.00000000",
      "frozen_btg": "0.00000000",
      "frozen_bts": "0.00000000",
      "frozen_doge": "0.00000000",
      "frozen_drk": "0.00000000",
      "frozen_etc": "0.00000000",
      "frozen_eth": "0.00000000",
      "frozen_idr": "0",
      "frozen_ignis": "0.00000000",
      "frozen_ltc": "0.00000000",
      "frozen_nem": "0.00000000",
      "frozen_nxt": "0.00000000",
      "frozen_str": "0.00000000",
      "frozen_waves": "0.00000000",
      "frozen_xrp": "0.00000000",
      "frozen_xzc": "0.00000000",
      "idr": "0",
      "ignis": "0.00000000",
      "ltc": "0.00000000",
      "nem": "0.00000000",
      "nxt": "0.00000000",
      "str": "0.00000000",
      "waves": "0.00000000",
      "xrp": "0.00000000",
      "xzc": "0.00000000"
    }
  }
}
```
&nbsp;

#### Trade History

> Menampilkan Riwayat Penjualan dan Pembelian

**Parameter**

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`count`|tidak|jumlah transaksi yang ingin ditampilkan|`int`|`1000`
`from_id`|tidak|id awal, range filter berdasarkan id transaksi| `int` | -
`end_id`|tidak|id akhir, range filter berdasarkan id transaksi| `int`| -
`since`|tidak|waktu awal, range filter berdasarkan waktu|`UNIX time`|-
`end`|tidak|waktu akhir, range filter berdasarkan waktu| `UNIX time`|-
`pair`|ya|pasangan simbol mata uang|`btc_idr`, `ltc_str`, `str_btc`, `doge_btc`, dll|`btc_idr`

contoh:

```python
>>> client.trade_history()
```

contoh dengan filter:

```python
>>> import time
>>> from datetime import datetime
>>> 
>>> since = int(time.mktime(datetime(2017,1,1).timetuple()))
>>> end = int(time.mktime(datetime.now().timetuple()))
>>> 
>>> client.trade_history('btc_idr', since=since, end=end)
```

return:

```json
{
  "success": 1,
  "return": {
    "trades": [
      {
        "btc": "0.00325521",
        "fee": "0",
        "order_id": "15257038",
        "price": "155500000",
        "trade_id": "5121170",
        "trade_time": "1512161070",
        "type": "sell"
      },
      {
        "btc": "0.00651041",
        "fee": "0",
        "order_id": "15258183",
        "price": "155000000",
        "trade_id": "5121059",
        "trade_time": "1512160945",
        "type": "sell"
      },
      {
        "btc": "0.01302083",
        "fee": "0",
        "order_id": "15249174",
        "price": "153600000",
        "trade_id": "5126773",
        "trade_time": "1512139191",
        "type": "buy"
      },
      {
        "btc": "0.00645994",
        "fee": "0",
        "order_id": "15246650",
        "price": "154800000",
        "trade_id": "5125085",
        "trade_time": "1512136323",
        "type": "buy"
      }
    ]
  }
}

```
&nbsp;

#### Open Orders

> Menampilkan order yang sedang terpasang (milik sendiri, jual/beli)

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`pair`|tidak|pasangan simbol mata uang|`btc_idr`, `ltc_str`, `str_btc`, `doge_btc`, dll| -

contoh:

```python
>>> client.open_orders() # menampilkan semua open order
>>> 
>>> client.open_orders('str_btc') # hanya menampilkan open order bitcoin - rupiah
```
&nbsp;

return:

```json
{
  "success": 1,
  "return": {
    "orders": [
      {
        "order_id": "4503907",
        "order_str": "281.52688775",
        "price": "0.00005000",
        "remain_str": "281.52688775",
        "submit_time": "1515699470",
        "type": "sell"
      },
      {
        "order_id": "4503909",
        "order_str": "281.52688776",
        "price": "0.00005500",
        "remain_str": "281.52688776",
        "submit_time": "1515699484",
        "type": "sell"
      }
    ]
  }
}
```

#### Order History

> Return list of order history (buy and sell)

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`pair`|tidak|pasangan simbol mata uang|`btc_idr`, `ltc_str`, `str_btc`, `doge_btc`, dll| -
`count`|tidak|jumlah riwayat yang ingin ditampilkan|`int`|100
`from`|tidak|offset jumlah yang ingin ditampilkan|`int`|0

contoh:

```python
>>> client.order_history()
```

return:

```json
{
  "success": 1,
  "return": {
    "orders": [
      {
        "order_id": "11512",
        "type": "sell",
        "price": "5000000",
        "submit_time": "1392227908",
        "finish_time": "1392227978",
        "status": "filled",
        "order_btc": "0.00100000",
        "remain_btc": "0.00000000"
      },
      {
        "order_id": "11513",
        "type": "buy",
        "price": "5000000",
        "submit_time": "1392227908",
        "finish_time": "1392227978",
        "status": "cancelled",
        "order_idr": "1000",
        "remain_idr": "1000"
      }
    ]
  }
}
```
&nbsp;

#### Get Order

> Menampilkan detil order

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`pair`|ya|pasangan simbol mata uang|`btc_idr`, `ltc_str`, `str_btc`, `doge_btc`, dll| -
`order_id`|ya|id unik order|`int`|-

contoh:

```python
>>> client.get_order(order_id=1111111, pair='ltc_btc')
```

return:

```json
// Jual
{
  "success": 1,
  "return": {
    "order": {
      "order_id": "94425",
      "price": "0.00810000",
      "type": "sell",
      "order_ltc": "1.00000000",
      "remain_ltc": "0.53000000",
      "submit_time": "1497657065",
      "finish_time": "0",
      "status": "open"
    }
  }
}
```

```json
// Beli
{
  "success": 1,
  "return": {
    "order": {
      "order_id": "664257",
      "price": "1000000000",
      "type": "buy",
      "order_rp": "10000",
      "remain_rp": "0",
      "submit_time": "1497330670",
      "finish_time": "1497330670",
      "status": "filled"
    }
  }
}
```
&nbsp;

#### Cancel Order

> Membatalkan order yang sedang terpasang

Parameter|Diperlukan|Keterangan|Nilai|Default
---|---|---|---|---
`pair`|ya|pasangan simbol mata uang|`btc_idr`, `ltc_str`, `str_btc`, `doge_btc`, dll| -
`order_id`|ya|id unik order|`int`|-
`trx_type`|ya|jenis transaksi|`buy` atau `sell`|-

contoh:

```python
>>> client.cancel_order(order_id=111111, pair='doge_btc', trx_type='sell')
```

return

```json
{
  "success": 1,
  "return": {
    "order_id": 11574,
    "type": "buy",
    "balance": {
      "idr": "5000000",
      "btc": 2.5,
      "ltc": 900.092,
      "doge": 1552.23,
      "xpy": 123.959
    }
  }
}

```

## Legal

Repository ini bukan merupakan repository official Bitcoin.co.id dan saya tidak terafiliasi dengan Bitcoin.co.id dalam pengembangannya. 

PyVIP dirilis dibawah lisensi MIT.




















