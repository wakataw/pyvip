from setuptools import setup

setup(
    name='pyvip',
    version='0.1.0',
    packages=['pyvip'],
    install_requires=['requests'],
    url='https://github.com/gonggle/PyVIP',
    license='MIT',
    author='Agung Pratama',
    author_email='me@agungpratama.id',
    description='Python API Wrapper for Bitcoin Indonesia',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Text Processing :: Linguistic',
        'Intended Audience :: Developers',
    ],
    keywords='bitcoin api-wrapper indonesia',
)
